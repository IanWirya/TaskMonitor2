package com.example.user.taskmonitor.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.taskmonitor.Apihelper.BaseApiService;
import com.example.user.taskmonitor.Apihelper.RetrofitClient;
import com.example.user.taskmonitor.R;
import com.example.user.taskmonitor.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateActivity extends AppCompatActivity {

    TextView TaskName;
    Button btn_update;
    CheckBox cekStatus;
    EditText ketTask;
    ProgressDialog loading;
    String taskID,taskStat;
    SessionManager session;
    Context context;
    BaseApiService.UpdateNEInterface updateTaskInterface;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        TaskName = (TextView)findViewById(R.id.taskName);
        cekStatus = (CheckBox)findViewById(R.id.cekStat);
        ketTask = (EditText)findViewById(R.id.taskKet);
        btn_update=(Button)findViewById(R.id.btn_update);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarupdate);
        toolbar.setTitle("Update Task");
        toolbar.setLogo(R.mipmap.ic_launcher);
        setSupportActionBar(toolbar);

        session = new SessionManager(getApplicationContext());

        final Bundle bundleTaskInfo = getIntent().getExtras();
        if(bundleTaskInfo != null){
            TaskName.setText(bundleTaskInfo.getString("Task_Name"));
            taskID = bundleTaskInfo.getString("Task_ID");
            String Jobnumtask = bundleTaskInfo.getString("JobnumTask");
        }

        context = this;
        updateTaskInterface = RetrofitClient.getClient().create(BaseApiService.UpdateNEInterface.class);
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading = ProgressDialog.show(context, null, "Please Wait...", true, false);
                postTaskUpdate();

            }
        });

    }

    private void postTaskUpdate() {
        if (cekStatus.isChecked()) {
            taskStat = "2";
        }else {
            taskStat = "1";
        }
        updateTaskInterface.updateTaskNE(taskID,taskStat,ketTask.getText().toString())
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            Log.i("debug","onResponse: SUCCESS");
                            loading.dismiss();
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("message").equals("Update Success")){
                                    Toast.makeText(context, "UPDATE SUCCESS", Toast.LENGTH_SHORT).show();
                                }else{
                                    //String error_msg = jsonRESULTS.getString("error_msg");
                                    Toast.makeText(context,"UPDATE FAILED",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else {
                            loading.dismiss();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("debug", "onFailure: ERROR >" + t.toString());
                        loading.dismiss();
                    }
                });
    }
    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        getMenuInflater().inflate(R.menu.menu_update,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if(id ==  R.id.action_logout){
            session.logoutUser();
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
