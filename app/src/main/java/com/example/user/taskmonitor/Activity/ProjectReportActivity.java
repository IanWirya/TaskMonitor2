package com.example.user.taskmonitor.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.taskmonitor.Apihelper.BaseApiService;
import com.example.user.taskmonitor.Apihelper.RetrofitClient;
import com.example.user.taskmonitor.R;
import com.example.user.taskmonitor.SessionManager;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;
import org.honorato.multistatetogglebutton.ToggleButton;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProjectReportActivity extends AppCompatActivity {

    BaseApiService.ProjectReportInterface projectReportInterface;
    ProgressDialog loading;
    Button btn_submit;
    EditText ketReport;
    TextView date;
    Context context;
    String projectID, projectStat;
    SessionManager session;
    private  String DEBUGTAG = "CustomButtonExample";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_report);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarreport);
        toolbar.setTitle("SUBMIT REPORT");
        toolbar.setLogo(R.mipmap.ic_launcher);
        setSupportActionBar(toolbar);

        session = new SessionManager(getApplicationContext());

        date = (TextView)findViewById(R.id.DateUpdate);
        ketReport = (EditText)findViewById(R.id.reportKet);
        btn_submit = (Button)findViewById(R.id.btn_submit);
        projectReportInterface = RetrofitClient.getClient().create(BaseApiService.ProjectReportInterface.class);

        final Calendar c = Calendar.getInstance();
        int yy = c.get(Calendar.YEAR);
        int mm = c.get(Calendar.MONTH);
        int dd = c.get(Calendar.DAY_OF_MONTH);
        date.setText(new StringBuilder().append(dd).append(" ").append("-").append(mm+1).append("-").append(yy));

        final Bundle bundleProjectInfo = getIntent().getExtras();
        if (bundleProjectInfo != null){
            projectID = bundleProjectInfo.getString("p_id");
        }


        MultiStateToggleButton button = (MultiStateToggleButton)this.findViewById(R.id.mstb_multi_id);
        button.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                Log.d(DEBUGTAG, "Position: " + value);
                if (value == 1){
                    projectStat = "1";
                }else if (value ==2){
                    projectStat = "2";
                }else {
                    projectStat = "0";
                }
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading = ProgressDialog.show(ProjectReportActivity.this, null, "Please Wait...", true, false);
                postProjectReport();
            }
        });
    }

    private void postProjectReport() {
        projectReportInterface.reportProject(projectID,projectStat,ketReport.getText().toString())
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            Log.i("debug","onResponse: SUCCESS");
                            loading.dismiss();
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("message").equals("Update Success")){
                                    Toast.makeText(getApplicationContext(), "REPORT SUCCESS", Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(ProjectReportActivity.this, MainActivity.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(i);
                                }else{
                                    Toast.makeText(getApplicationContext(),"REPORT FAILED",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else {
                            loading.dismiss();
                        }
                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("debug", "onFailure: ERROR >" + t.toString());
                        loading.dismiss();
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        getMenuInflater().inflate(R.menu.menu_report,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_logout) {
            session.logoutUser();
            finish();
        }
        return super.onOptionsItemSelected(item);

    }
}
