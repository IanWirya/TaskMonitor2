package com.example.user.taskmonitor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.taskmonitor.Apihelper.BaseApiService;
import com.example.user.taskmonitor.Model.ResTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user on 05-Feb-18.
 */

public class TaskAdapter extends RecyclerView.Adapter <TaskAdapter.TaskViewHolder>{


    private List<ResTask> resTasks;
    private Context mcontext;

    public TaskAdapter(List<ResTask> resTasks, Context context){
        this.resTasks = resTasks;
        this.mcontext = context;
    }
    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_layout,parent,false);

        return new TaskViewHolder(view);
    }
    @Override
    public void onBindViewHolder(final TaskViewHolder holder, int position) {
        holder.taskName.setText(resTasks.get(position).getTaskName());
        holder.taskKet.setText(resTasks.get(position).getKeteranganNE());



    }

    @Override
    public int getItemCount() {
        return resTasks.size();
    }

    public  class TaskViewHolder extends RecyclerView.ViewHolder{

        TextView taskName,taskKet;
        CheckBox cekTask;
        public TaskViewHolder(View itemView) {
            super(itemView);

            taskName = (TextView)itemView.findViewById(R.id.nameTask);
            taskKet = (TextView)itemView.findViewById(R.id.ketTask);


        }
    }
}
