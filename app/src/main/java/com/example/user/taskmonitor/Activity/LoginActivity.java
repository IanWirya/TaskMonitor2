package com.example.user.taskmonitor.Activity;

/**
 * Created by user on 04-Jan-18.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.user.taskmonitor.Apihelper.BaseApiService;
import com.example.user.taskmonitor.Apihelper.RetrofitClient;
import com.example.user.taskmonitor.R;
import com.example.user.taskmonitor.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    // UI references.
    Button  btLogin;
    EditText userName, passWord;

    SessionManager session;
    ProgressDialog loading;
    Context mContext;
    BaseApiService.LoginInterface mApiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        session = new SessionManager(getApplicationContext());

        btLogin = (Button)findViewById(R.id.btnLogin);
        userName = (EditText)findViewById(R.id.textUser);
        passWord = (EditText)findViewById(R.id.textPass);

        mContext = this;
        mApiService = RetrofitClient.getClient().create(BaseApiService.LoginInterface.class);
        btLogin.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                    loading = ProgressDialog.show(mContext, null, "Please Wait...", true, false);
                    requestLogin();
            }
        });
    }

    private void requestLogin() {
    mApiService.loginRequest(userName.getText().toString(),passWord.getText().toString())
            .enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()){
                        Log.i("debug","onResponse: SUCCESS");
                        loading.dismiss();
                        try {
                            JSONObject jsonRESULTS = new JSONObject(response.body().string());
                            if (jsonRESULTS.getString("error").equals("false")){
                                Toast.makeText(mContext, "LOGIN SUCCESS", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);

                                session.createLoginSession(jsonRESULTS.getJSONObject("user").getString("nama"),jsonRESULTS.getJSONObject("user").getString("engineerid"));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }else{
                                String error_msg = jsonRESULTS.getString("error_msg");
                                Toast.makeText(mContext,error_msg,Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }else {
                        loading.dismiss();
                    }
                }


                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Log.e("debug", "onFailure: ERROR >" + t.toString());
                    loading.dismiss();
                }
            });
    }
}

