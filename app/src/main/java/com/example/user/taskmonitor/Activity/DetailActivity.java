package com.example.user.taskmonitor.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.user.taskmonitor.R;
import com.example.user.taskmonitor.SessionManager;

public class DetailActivity extends AppCompatActivity {


    TextView ProjectNum,ProjectName,NEnumber,NEname,NEaddress,NEtower,NEheight,NEgps,FEnumber,FEname,FEaddress,FEtower,FEheight,FEgps;
    Button btn_NE,btn_FE;
    SessionManager session;
    String projectNumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ProjectNum = (TextView)findViewById(R.id.noProject);
        ProjectName = (TextView)findViewById(R.id.nameProject);
        NEnumber = (TextView)findViewById(R.id.NESnumber);
        NEname = (TextView)findViewById(R.id.NESname);
        NEaddress = (TextView)findViewById(R.id.NESaddress);
        NEtower = (TextView)findViewById(R.id.NETypeTower);
        NEheight = (TextView)findViewById(R.id.NEHeightTower);
        NEgps = (TextView)findViewById(R.id.NEGPSposition);
        FEnumber = (TextView)findViewById(R.id.FESnumber);
        FEname = (TextView)findViewById(R.id.FESname);
        FEaddress = (TextView)findViewById(R.id.FESaddress);
        FEtower = (TextView)findViewById(R.id.FETypeTower);
        FEheight = (TextView)findViewById(R.id.FEHeightTower);
        FEgps = (TextView)findViewById(R.id.FEGPSposition);
        btn_NE = (Button)findViewById(R.id.btn_NearEnd);
        btn_FE = (Button)findViewById(R.id.btn_FarEnd);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbardetail);
        toolbar.setLogo(R.mipmap.ic_launcher);
        toolbar.setTitle("Detail Proyek");
        setSupportActionBar(toolbar);

        session = new SessionManager(getApplicationContext());


        final Bundle bundleSendProjectValue = getIntent().getExtras();
        if (bundleSendProjectValue != null)
        {
            ProjectNum.setText(bundleSendProjectValue.getString("Project_Number"));
            projectNumber = bundleSendProjectValue.getString("Project_Number");
            ProjectName.setText(bundleSendProjectValue.getString("Project_Name"));
            NEnumber.setText(bundleSendProjectValue.getString("NeSite_Number"));
            NEname.setText(bundleSendProjectValue.getString("NeSite_Name"));
            NEaddress.setText(bundleSendProjectValue.getString("NeSite_Address"));
            NEtower.setText(bundleSendProjectValue.getString("NeType_Tower"));
            NEheight.setText(bundleSendProjectValue.getString("NeTower_Height"));
            NEgps.setText(bundleSendProjectValue.getString("NeGPS_Position"));
            FEnumber.setText(bundleSendProjectValue.getString("FeSite_Number"));
            FEname.setText(bundleSendProjectValue.getString("FeSite_Name"));
            FEaddress.setText(bundleSendProjectValue.getString("FeSite_Address"));
            FEtower.setText(bundleSendProjectValue.getString("FeType_Tower"));
            FEheight.setText(bundleSendProjectValue.getString("FeTower_Height"));
            FEgps.setText(bundleSendProjectValue.getString("FeGPS_Position"));

        }


        btn_NE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DetailActivity.this, TaskActivity.class);
                Bundle bundleSendNearEnd = new Bundle();
                bundleSendNearEnd.putString("Jobnum",bundleSendProjectValue.getString("Project_Number"));
                bundleSendNearEnd.putString("TaskTitle","Near End");
                bundleSendNearEnd.putString("p_id",bundleSendProjectValue.getString("Project_ID"));
                bundleSendNearEnd.putString("p_status",bundleSendProjectValue.getString("Project_Status"));
                i.putExtras(bundleSendNearEnd);
                startActivity(i);
            }
        });


        btn_FE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DetailActivity.this, TaskFeActivity.class);
                Bundle bundleSendFarEnd = new Bundle();
                bundleSendFarEnd.putString("Jobnum",bundleSendProjectValue.getString("Project_Number"));
                bundleSendFarEnd.putString("TaskTitle","Far End");
                bundleSendFarEnd.putString("p_id",bundleSendProjectValue.getString("Project_ID"));
                bundleSendFarEnd.putString("p_status",bundleSendProjectValue.getString("Project_Status"));
                i.putExtras(bundleSendFarEnd);
                startActivity(i);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        getMenuInflater().inflate(R.menu.menu_detail,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if(id ==  R.id.action_logout) {
            session.logoutUser();
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
