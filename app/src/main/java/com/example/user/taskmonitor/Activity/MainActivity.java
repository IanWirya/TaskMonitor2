package com.example.user.taskmonitor.Activity;

import android.app.ListActivity;
import android.content.ComponentName;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.taskmonitor.R;
import com.example.user.taskmonitor.SessionManager;

import java.util.HashMap;

import static com.google.android.gms.analytics.internal.zzy.v;

public class MainActivity extends AppCompatActivity {
    TextView tvResultNama;
    String resultNama, engineerID;
    Button bPending, bCancel, bDone;
    SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Task Monitor");
        toolbar.setLogo(R.mipmap.ic_launcher);
        setSupportActionBar(toolbar);

        tvResultNama = (TextView)findViewById(R.id.tvResultNama);
        bPending = (Button)findViewById(R.id.btn_pending);
        bCancel = (Button) findViewById(R.id.btn_cancel);
        bDone = (Button) findViewById(R.id.btn_done);



        session = new SessionManager(getApplicationContext());
        session.checkLosgin();
        HashMap<String,String> user = session.getUserDetails();
        resultNama = user.get(SessionManager.KEY_NAME);
        engineerID = user.get(SessionManager.KEY_ID);
        tvResultNama.setText(resultNama);
        /*
        Bundle extras = getIntent().getExtras();
        if (extras != null){
            resultNama = extras.getString("userNama");
            engineerID = extras.getString("engID");
            tvResultNama.setText(resultNama);

        }*/

        bPending.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setComponent(new ComponentName("com.example.user.taskmonitor","com.example.user.taskmonitor.Activity.ListActivity"));
                i.putExtra("engineerid",engineerID);
                startActivity(i);
            }
        });

        bCancel.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setComponent(new ComponentName("com.example.user.taskmonitor","com.example.user.taskmonitor.Activity.ListActivity"));
                i.putExtra("engineerid",engineerID);
                startActivity(i);
            }
        });

        bDone.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setComponent(new ComponentName("com.example.user.taskmonitor","com.example.user.taskmonitor.Activity.ListActivity"));
                i.putExtra("engineerid",engineerID);
                startActivity(i);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if(id ==  R.id.action_logout){
            session.logoutUser();
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
