package com.example.user.taskmonitor.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.taskmonitor.Apihelper.BaseApiService;
import com.example.user.taskmonitor.Apihelper.RetrofitClient;
import com.example.user.taskmonitor.Model.ResTask;
import com.example.user.taskmonitor.ProjectAdapter;
import com.example.user.taskmonitor.R;
import com.example.user.taskmonitor.SessionManager;
import com.example.user.taskmonitor.TaskAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaskActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<ResTask> resTasks;
    private TaskAdapter adapter;
    private BaseApiService.TaskProjectInterface taskInterface;
    private BaseApiService.CheckAllTaskInterface checkAllTaskInterface;
    private TextView jobNumber ,title_task;
    private Button btn_lanjut;
    private String Jobnum;
    private SessionManager session;
    ProgressDialog loading;

    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbartask);
        toolbar.setTitle("DAFTAR TASK NEAR END");
        toolbar.setLogo(R.mipmap.ic_launcher);
        setSupportActionBar(toolbar);

        session = new SessionManager(getApplicationContext());

        btn_lanjut = (Button)findViewById(R.id.btn_proceedNE);
        title_task = (TextView) findViewById(R.id.taskTitle);
        jobNumber = (TextView)findViewById(R.id.taskJobNum);
        recyclerView = (RecyclerView) findViewById(R.id.taskRecView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListenerTask(this, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent i = new Intent(TaskActivity.this, UpdateActivity.class);
                Bundle bundleSendTaskValue = new Bundle();
                bundleSendTaskValue.putString("Task_ID",resTasks.get(position).getId());
                bundleSendTaskValue.putString("Task_Name",resTasks.get(position).getTaskName());
                bundleSendTaskValue.putString("FE_Status",resTasks.get(position).getFEStatus());
                bundleSendTaskValue.putString("NE_Status",resTasks.get(position).getNEStatus());
                bundleSendTaskValue.putString("Task_Ket",resTasks.get(position).getKeteranganNE());
                bundleSendTaskValue.putString("JobnumTask",Jobnum);
                i.putExtras(bundleSendTaskValue);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }

            @Override
            public void onLongClick(View view, int position) {
                Toast.makeText(TaskActivity.this, "Long press on position :"+(position+1), Toast.LENGTH_LONG).show();
            }
        }));

        final Bundle bundleSendTaskTitle = getIntent().getExtras();
        if (bundleSendTaskTitle != null)
        {

            Jobnum = bundleSendTaskTitle.getString("Jobnum");
            title_task.setText(bundleSendTaskTitle.getString("TaskTitle"));
            jobNumber.setText(Jobnum);
            getTask(Jobnum);
        }

        btn_lanjut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAllTaskInterface = RetrofitClient.getClient().create(BaseApiService.CheckAllTaskInterface.class);
                checkAllTaskInterface.cekTask(Jobnum)
                        .enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful()){
                                    Log.i("debug","onResponse: SUCCESS");
                                    try{
                                        JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                        if (jsonRESULTS.getString("code").equals("1")){
                                            String berhasil = jsonRESULTS.getString("message");
                                            Toast.makeText(getApplicationContext(), berhasil, Toast.LENGTH_SHORT).show();
                                            Intent i = new Intent(TaskActivity.this, ProjectReportActivity.class);
                                            Bundle bundleReport = new Bundle();
                                            bundleReport.putString("p_id",bundleSendTaskTitle.getString("p_id"));
                                            bundleReport.putString("p_status",bundleSendTaskTitle.getString("p_status"));
                                            i.putExtras(bundleReport);
                                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(i);
                                        }else{
                                            String error_msg = jsonRESULTS.getString("message");
                                            Toast.makeText(getApplicationContext(),error_msg,Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }else {
                                }
                            }
                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Log.e("debug", "onFailure: ERROR >" + t.toString());
                            }
                        });
            }
        });
    }

    public void getTask(String jobnum)
    {
        taskInterface = RetrofitClient.getClient().create(BaseApiService.TaskProjectInterface.class);
        Call<List<ResTask>> call = taskInterface.getTaskList(jobnum);
        call.enqueue(new Callback<List<ResTask>>() {
            @Override
            public void onResponse(Call<List<ResTask>> call, Response<List<ResTask>> response) {
               if (response.body() != null)
               {
                   resTasks = response.body();
                   adapter = new TaskAdapter(resTasks,TaskActivity.this);
                   recyclerView.setAdapter(adapter);
               }else {
                   Toast.makeText(context,"TASK KOSONG",Toast.LENGTH_SHORT).show();
               }

            }

            @Override
            public void onFailure(Call<List<ResTask>> call, Throwable t) {
                Log.e("debug", "onFailure: ERROR >" + t.toString());
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        getMenuInflater().inflate(R.menu.menu_task,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if(id ==  R.id.action_logout){
            session.logoutUser();
            finish();
        }else if (id == R.id.action_reconnect){
            getTask(Jobnum);
        }

        return super.onOptionsItemSelected(item);
    }
    public static interface ClickListener{
        public void onClick(View view, int position);
        public void onLongClick(View view,int position);
    }

    class RecyclerTouchListenerTask implements RecyclerView.OnItemTouchListener {

        private ClickListener clicklistener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListenerTask(Context context, final RecyclerView recycleView, final ClickListener clicklistener){

            this.clicklistener=clicklistener;
            gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child=recycleView.findChildViewUnder(e.getX(),e.getY());
                    if(child!=null && clicklistener!=null){
                        clicklistener.onLongClick(child,recycleView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child=rv.findChildViewUnder(e.getX(),e.getY());
            if(child!=null && clicklistener!=null && gestureDetector.onTouchEvent(e)){
                clicklistener.onClick(child,rv.getChildAdapterPosition(child));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
