package com.example.user.taskmonitor.Model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResTask {
    private boolean isSelected;

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("TaskName")
    @Expose
    private String taskName;
    @SerializedName("JobNumberTask")
    @Expose
    private String jobNumberTask;
    @SerializedName("KeteranganNE")
    @Expose
    private String keteranganNE;
    @SerializedName("KeteranganFE")
    @Expose
    private String keteranganFE;
    @SerializedName("NEStatus")
    @Expose
    private String nEStatus;
    @SerializedName("FEStatus")
    @Expose
    private String fEStatus;
    @SerializedName("Creationdate")
    @Expose
    private String creationdate;
    @SerializedName("UpdationDate")
    @Expose
    private String updationDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ResTask withId(String id) {
        this.id = id;
        return this;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public ResTask withTaskName(String taskName) {
        this.taskName = taskName;
        return this;
    }

    public String getJobNumberTask() {
        return jobNumberTask;
    }

    public void setJobNumberTask(String jobNumberTask) {
        this.jobNumberTask = jobNumberTask;
    }

    public ResTask withJobNumberTask(String jobNumberTask) {
        this.jobNumberTask = jobNumberTask;
        return this;
    }

    public String getKeteranganNE() {
        return keteranganNE;
    }

    public void setKeteranganNE(String keteranganNE) {
        this.keteranganNE = keteranganNE;
    }

    public ResTask withKeteranganNE(String keteranganNE) {
        this.keteranganNE = keteranganNE;
        return this;
    }

    public String getKeteranganFE() {
        return keteranganFE;
    }

    public void setKeteranganFE(String keteranganFE) {
        this.keteranganFE = keteranganFE;
    }

    public ResTask withKeteranganFE(String keteranganFE) {
        this.keteranganFE = keteranganFE;
        return this;
    }

    public String getNEStatus() {
        return nEStatus;
    }

    public void setNEStatus(String nEStatus) {
        this.nEStatus = nEStatus;
    }

    public ResTask withNEStatus(String nEStatus) {
        this.nEStatus = nEStatus;
        return this;
    }

    public String getFEStatus() {
        return fEStatus;
    }

    public void setFEStatus(String fEStatus) {
        this.fEStatus = fEStatus;
    }

    public ResTask withFEStatus(String fEStatus) {
        this.fEStatus = fEStatus;
        return this;
    }

    public String getCreationdate() {
        return creationdate;
    }

    public void setCreationdate(String creationdate) {
        this.creationdate = creationdate;
    }

    public ResTask withCreationdate(String creationdate) {
        this.creationdate = creationdate;
        return this;
    }

    public String getUpdationDate() {
        return updationDate;
    }

    public void setUpdationDate(String updationDate) {
        this.updationDate = updationDate;
    }

    public ResTask withUpdationDate(String updationDate) {
        this.updationDate = updationDate;
        return this;
    }


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}