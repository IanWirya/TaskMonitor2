package com.example.user.taskmonitor.Apihelper;
import com.example.user.taskmonitor.Model.ResProject;
import com.example.user.taskmonitor.Model.ResTask;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by user on 04-Feb-18.
 */

public interface BaseApiService {

    public interface LoginInterface{
        @FormUrlEncoded
        @POST("login.php")
        Call<ResponseBody> loginRequest(@Field("username") String username,
                                        @Field("password")String password);
    }
    public interface ListProjectInterface{
        @GET("projectlist.php")
        Call<List<ResProject>> getProjectList(@Query("engineerid") String engineerID);
    }

    public interface TaskProjectInterface{
        @GET("tasklist.php")
        Call<List<ResTask>> getTaskList(@Query("jobnum")String jobNum);
    }

    public interface UpdateNEInterface{
        @FormUrlEncoded
        @POST("neupdatetask.php")
        Call<ResponseBody> updateTaskNE(@Field("taskid")String taskid,
                                        @Field("nestatus")String nestatus,
                                        @Field("keteranganne")String keterangan);
    }

    public interface UpdateFEInterface{
        @FormUrlEncoded
        @POST("feupdatetask.php")
        Call<ResponseBody> updateTaskFE(@Field("taskid")String taskid,
                                        @Field("festatus")String festatus,
                                        @Field("keteranganfe")String keterangan);
    }

    public interface CheckAllTaskInterface{
        @GET("netasklistcek.php")
        Call<ResponseBody> cekTask(@Query("jobnum")String jobNum);
    }

    public interface CheckAllTaskFEInterface{
        @GET("fetasklistcek.php")
        Call<ResponseBody> cekTask(@Query("jobnum")String jobNum);
    }
    public  interface ProjectReportInterface{
        @FormUrlEncoded
        @POST("updateproject.php")
        Call<ResponseBody> reportProject(@Field("projectid")String projectid,
                                         @Field("projectstatus")String projectstatus,
                                         @Field("projectnote")String projectnote);
    }



}
