package com.example.user.taskmonitor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 04-Feb-18.
 */

public class ResProject {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("JobNumber")
    @Expose
    private String jobNumber;
    @SerializedName("ProjectName")
    @Expose
    private String projectName;
    @SerializedName("NeSiteNumber")
    @Expose
    private String neSiteNumber;
    @SerializedName("NeSiteName")
    @Expose
    private String neSiteName;
    @SerializedName("NeSiteAddress")
    @Expose
    private String neSiteAddress;
    @SerializedName("NeTypeofTower")
    @Expose
    private String neTypeofTower;
    @SerializedName("NeTowerHeight")
    @Expose
    private String neTowerHeight;
    @SerializedName("NeGPSposition")
    @Expose
    private String neGPSposition;
    @SerializedName("FeSiteNumber")
    @Expose
    private String feSiteNumber;
    @SerializedName("FeSiteName")
    @Expose
    private String feSiteName;
    @SerializedName("FeSiteAddress")
    @Expose
    private String feSiteAddress;
    @SerializedName("FeTypeofTower")
    @Expose
    private String feTypeofTower;
    @SerializedName("FeTowerHeight")
    @Expose
    private String feTowerHeight;
    @SerializedName("FeGPSposition")
    @Expose
    private String feGPSposition;
    @SerializedName("Engineer")
    @Expose
    private String engineer;
    @SerializedName("CreationDate")
    @Expose
    private String creationDate;
    @SerializedName("UpdationDate")
    @Expose
    private String updationDate;
    @SerializedName("status")
    @Expose
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ResProject withId(String id) {
        this.id = id;
        return this;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public ResProject withJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
        return this;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public ResProject withProjectName(String projectName) {
        this.projectName = projectName;
        return this;
    }

    public String getNeSiteNumber() {
        return neSiteNumber;
    }

    public void setNeSiteNumber(String neSiteNumber) {
        this.neSiteNumber = neSiteNumber;
    }

    public ResProject withNeSiteNumber(String neSiteNumber) {
        this.neSiteNumber = neSiteNumber;
        return this;
    }

    public String getNeSiteName() {
        return neSiteName;
    }

    public void setNeSiteName(String neSiteName) {
        this.neSiteName = neSiteName;
    }

    public ResProject withNeSiteName(String neSiteName) {
        this.neSiteName = neSiteName;
        return this;
    }

    public String getNeSiteAddress() {
        return neSiteAddress;
    }

    public void setNeSiteAddress(String neSiteAddress) {
        this.neSiteAddress = neSiteAddress;
    }

    public ResProject withNeSiteAddress(String neSiteAddress) {
        this.neSiteAddress = neSiteAddress;
        return this;
    }

    public String getNeTypeofTower() {
        return neTypeofTower;
    }

    public void setNeTypeofTower(String neTypeofTower) {
        this.neTypeofTower = neTypeofTower;
    }

    public ResProject withNeTypeofTower(String neTypeofTower) {
        this.neTypeofTower = neTypeofTower;
        return this;
    }

    public String getNeTowerHeight() {
        return neTowerHeight;
    }

    public void setNeTowerHeight(String neTowerHeight) {
        this.neTowerHeight = neTowerHeight;
    }

    public ResProject withNeTowerHeight(String neTowerHeight) {
        this.neTowerHeight = neTowerHeight;
        return this;
    }

    public String getNeGPSposition() {
        return neGPSposition;
    }

    public void setNeGPSposition(String neGPSposition) {
        this.neGPSposition = neGPSposition;
    }

    public ResProject withNeGPSposition(String neGPSposition) {
        this.neGPSposition = neGPSposition;
        return this;
    }

    public String getFeSiteNumber() {
        return feSiteNumber;
    }

    public void setFeSiteNumber(String feSiteNumber) {
        this.feSiteNumber = feSiteNumber;
    }

    public ResProject withFeSiteNumber(String feSiteNumber) {
        this.feSiteNumber = feSiteNumber;
        return this;
    }

    public String getFeSiteName() {
        return feSiteName;
    }

    public void setFeSiteName(String feSiteName) {
        this.feSiteName = feSiteName;
    }

    public ResProject withFeSiteName(String feSiteName) {
        this.feSiteName = feSiteName;
        return this;
    }

    public String getFeSiteAddress() {
        return feSiteAddress;
    }

    public void setFeSiteAddress(String feSiteAddress) {
        this.feSiteAddress = feSiteAddress;
    }

    public ResProject withFeSiteAddress(String feSiteAddress) {
        this.feSiteAddress = feSiteAddress;
        return this;
    }

    public String getFeTypeofTower() {
        return feTypeofTower;
    }

    public void setFeTypeofTower(String feTypeofTower) {
        this.feTypeofTower = feTypeofTower;
    }

    public ResProject withFeTypeofTower(String feTypeofTower) {
        this.feTypeofTower = feTypeofTower;
        return this;
    }

    public String getFeTowerHeight() {
        return feTowerHeight;
    }

    public void setFeTowerHeight(String feTowerHeight) {
        this.feTowerHeight = feTowerHeight;
    }

    public ResProject withFeTowerHeight(String feTowerHeight) {
        this.feTowerHeight = feTowerHeight;
        return this;
    }

    public String getFeGPSposition() {
        return feGPSposition;
    }

    public void setFeGPSposition(String feGPSposition) {
        this.feGPSposition = feGPSposition;
    }

    public ResProject withFeGPSposition(String feGPSposition) {
        this.feGPSposition = feGPSposition;
        return this;
    }

    public String getEngineer() {
        return engineer;
    }

    public void setEngineer(String engineer) {
        this.engineer = engineer;
    }

    public ResProject withEngineer(String engineer) {
        this.engineer = engineer;
        return this;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public ResProject withCreationDate(String creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public String getUpdationDate() {
        return updationDate;
    }

    public void setUpdationDate(String updationDate) {
        this.updationDate = updationDate;
    }

    public ResProject withUpdationDate(String updationDate) {
        this.updationDate = updationDate;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ResProject withStatus(String status) {
        this.status = status;
        return this;
    }
}
