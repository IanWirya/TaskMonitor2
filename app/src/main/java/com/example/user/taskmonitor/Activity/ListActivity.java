package com.example.user.taskmonitor.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.example.user.taskmonitor.Apihelper.BaseApiService;
import com.example.user.taskmonitor.Apihelper.RetrofitClient;
import com.example.user.taskmonitor.Model.ResProject;
import com.example.user.taskmonitor.ProjectAdapter;
import com.example.user.taskmonitor.R;
import com.example.user.taskmonitor.SessionManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListActivity extends AppCompatActivity  {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<ResProject> resProjects;
    private ProjectAdapter adapter;
    private BaseApiService.ListProjectInterface baseApiService;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarlist);
        toolbar.setTitle("DAFTAR PROYEK");
        toolbar.setLogo(R.mipmap.ic_launcher);
        setSupportActionBar(toolbar);

        session = new SessionManager(getApplicationContext());
        recyclerView = (RecyclerView) findViewById(R.id.listRecView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent i = new Intent(ListActivity.this, DetailActivity.class);
                Bundle bundleSendProjectValue = new Bundle();
                bundleSendProjectValue.putString("Project_ID",resProjects.get(position).getId());
                bundleSendProjectValue.putString("Project_Status",resProjects.get(position).getStatus());
                bundleSendProjectValue.putString("Project_Number", resProjects.get(position).getJobNumber());
                bundleSendProjectValue.putString("Project_Name",resProjects.get(position).getProjectName());
                bundleSendProjectValue.putString("NeSite_Number",resProjects.get(position).getNeSiteNumber());
                bundleSendProjectValue.putString("NeSite_Name",resProjects.get(position).getNeSiteName());
                bundleSendProjectValue.putString("NeSite_Address",resProjects.get(position).getNeSiteAddress());
                bundleSendProjectValue.putString("NeType_Tower",resProjects.get(position).getNeTypeofTower());
                bundleSendProjectValue.putString("NeTower_Height",resProjects.get(position).getNeTowerHeight());
                bundleSendProjectValue.putString("NeGPS_Position",resProjects.get(position).getNeGPSposition());
                bundleSendProjectValue.putString("FeSite_Number",resProjects.get(position).getFeSiteNumber());
                bundleSendProjectValue.putString("FeSite_Name",resProjects.get(position).getFeSiteName());
                bundleSendProjectValue.putString("FeSite_Address",resProjects.get(position).getFeSiteAddress());
                bundleSendProjectValue.putString("FeType_Tower",resProjects.get(position).getFeTypeofTower());
                bundleSendProjectValue.putString("FeTower_Height",resProjects.get(position).getFeTowerHeight());
                bundleSendProjectValue.putString("FeGPS_Position",resProjects.get(position).getFeGPSposition());
                i.putExtras(bundleSendProjectValue);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }

            @Override
            public void onLongClick(View view, int position) {
                Toast.makeText(ListActivity.this, "Long press on position :"+(position+1), Toast.LENGTH_LONG).show();
            }
        }));

        if(getIntent().getExtras() != null)
        {
            String engineerId = getIntent().getExtras().getString("engineerid");
            getInfo(engineerId);
        }

    }

    public void getInfo (String engineerId)
    {
        baseApiService = RetrofitClient.getClient().create(BaseApiService.ListProjectInterface.class);
        Call<List<ResProject>> call = baseApiService.getProjectList(engineerId);
        call.enqueue(new Callback<List<ResProject>>() {
            @Override
            public void onResponse(Call<List<ResProject>> call, Response<List<ResProject>> response) {
                resProjects = response.body();
                adapter = new ProjectAdapter(resProjects,ListActivity.this);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<ResProject>> call, Throwable t) {
                Log.e("debug", "onFailure: ERROR >" + t.toString());

            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        getMenuInflater().inflate(R.menu.menu_list,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if(id ==  R.id.action_logout){
            session.logoutUser();
            finish();
        }else if (id == R.id.action_search){
            session.logoutUser();
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
    public static interface ClickListener{
        public void onClick(View view, int position);
        public void onLongClick(View view,int position);
    }

    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

        private ClickListener clicklistener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final ClickListener clicklistener){

            this.clicklistener=clicklistener;
            gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child=recycleView.findChildViewUnder(e.getX(),e.getY());
                    if(child!=null && clicklistener!=null){
                        clicklistener.onLongClick(child,recycleView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child=rv.findChildViewUnder(e.getX(),e.getY());
            if(child!=null && clicklistener!=null && gestureDetector.onTouchEvent(e)){
                clicklistener.onClick(child,rv.getChildAdapterPosition(child));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

}
