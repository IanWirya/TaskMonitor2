package com.example.user.taskmonitor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.user.taskmonitor.Model.ResTask;

import java.util.List;

/**
 * Created by user on 22-Feb-18.
 */

public class TaskAdapterFE extends RecyclerView.Adapter <TaskAdapterFE.TaskViewHolder>{

    private List<ResTask> resTasks;
    private Context mcontext;

    public TaskAdapterFE(List<ResTask> resTasks, Context context){
        this.resTasks = resTasks;
        this.mcontext = context;
    }

    @Override
    public TaskAdapterFE.TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_layout,parent,false);

        return new TaskAdapterFE.TaskViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, int position) {
        holder.taskName.setText(resTasks.get(position).getTaskName());
        holder.taskKet.setText(resTasks.get(position).getKeteranganFE());
    }


    @Override
    public int getItemCount() {
        return resTasks.size();
    }

    public  class TaskViewHolder extends RecyclerView.ViewHolder{

        TextView taskName,taskKet;
        CheckBox cekTask;
        public TaskViewHolder(View itemView) {
            super(itemView);

            taskName = (TextView)itemView.findViewById(R.id.nameTask);
            taskKet = (TextView)itemView.findViewById(R.id.ketTask);


        }
    }
}
