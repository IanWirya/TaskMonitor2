package com.example.user.taskmonitor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.user.taskmonitor.Model.ResProject;

import java.util.List;

/**
 * Created by user on 05-Feb-18.
 */

public class ProjectAdapter extends RecyclerView.Adapter <ProjectAdapter.MyViewHolder> {

    private List<ResProject> resProjects;
    private Context context;

    public ProjectAdapter(List<ResProject> resProjects, Context context){
        this.resProjects = resProjects;
        this.context = context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.jobNum.setText(resProjects.get(position).getJobNumber());
        holder.jobName.setText(resProjects.get(position).getProjectName());
    }

    @Override
    public int getItemCount() {
        return resProjects.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView jobNum,jobName;
        public MyViewHolder(View itemView) {
            super(itemView);
            jobNum = (TextView)itemView.findViewById(R.id.jobNumber);
            jobName = (TextView)itemView.findViewById(R.id.jobName);
        }
    }
}
